import { Component, OnInit } from '@angular/core';
import { Users } from '../users';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrls: ['./user-crud.component.css']
})
export class UserCrudComponent implements OnInit {
user:Array<Users>=[];
deleteMsg:string=""
updateMsg:string=""
flag:boolean=false
uid:number=0
name:string=""
email:string=""
  constructor(public userdis:UserserviceService) { }

  ngOnInit(): void {
    this.loadUser();
  }
  loadUser():void{
    console.log("event fired")
    this.userdis.LoaduserDetails().subscribe(res=>this.user=res)
  }
  deleteUser(uid:number){
  console.log(uid)
  this.userdis.deleteUser(uid).subscribe(res=>this.deleteMsg=res,()=>this.loadUser)
  }
  updateUser(uu:Users){
   // console.log(uu)
   this.flag=true
   this.uid=uu.uid
   this.email=uu.email
   this.name=uu.name
  }
  updateUserName(){
    let uu={"uid":this.uid,"email":this.email,"name":this.name}
    this.userdis.updateuserDetails(uu).subscribe(res=>this.updateMsg=res,
      ()=>{
        this.loadUser()
        this.flag=false
      })
  }
}
