import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminserviceService } from '../adminservice.service';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {
  adminLoginRef=new FormGroup({
    email:new FormControl(),
    passward:new FormControl()
  })
  constructor(public admin:AdminserviceService, public router:Router) { }
userss:string="";
  ngOnInit(): void {
  }
  checkUser(){
    let loginu=this.adminLoginRef.value
    console.log(loginu)
    // this.admin.AdminSignupDetails(adminRef.value).subscribe(res=>{
    //   this.adminMsg=res;
    //   console.log(res)
    //   this.router.navigate(["adminlogin"])
    // })
    this.admin.adminLoginDetails(loginu).subscribe(res=>
      {
        this.userss=res;
        if(res=="done"){
          this.router.navigate(["admin-operation"])
        }
      })

  }
}
