import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { Admin } from './admin';


@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {

  constructor(public http:HttpClient) { }
  AdminSignupDetails(admin:AdminserviceService):Observable<string>{
  return this.http.post("http://localhost:8181/admin/registeradmin",admin,{responseType:'text'})
 
    }
    
   adminLoginDetails(admin:AdminserviceService):Observable<string>{
    return this.http.post("http://localhost:8181/admin/loginadmin",admin,{responseType:'text'})
   }
}
