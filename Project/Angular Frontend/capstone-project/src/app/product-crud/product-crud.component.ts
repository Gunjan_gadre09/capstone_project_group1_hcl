import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Product } from '../product';
import { ProductserviceService } from '../productservice.service';

@Component({
  selector: 'app-product-crud',
  templateUrl: './product-crud.component.html',
  styleUrls: ['./product-crud.component.css']
})
export class ProductCrudComponent implements OnInit {

  constructor(public productdis:ProductserviceService) { }
  product:Array<Product>=[];
  deleteMsg:string=""
  updateMsg:string=""
  productMsg:string=""
  flag:boolean=false
  flag1:boolean=false
  pid:number=0
  pname:string=""
  price:number=0
  prodquantity:number=0
  url:string=""
  ngOnInit(): void {
    this.loadProduct();
  }
  loadProduct():void{
    console.log("event fired")
    this.productdis.LoadProductDetails().subscribe(res=>{
      this.product=res
      console.log(res)
    })

  }
  deleteProduct(pid:number){
  console.log(pid)
  this.productdis.deleteProductDetails(pid).subscribe(res=>this.deleteMsg=res)
  }
  updateProduct(uu:Product){
    console.log(uu)
   this.flag=true
   this.pid=uu.pid
   this.pname=uu.pname
   this.price=uu.price
   this.prodquantity=uu.prodquantity
   this.url=uu.url
  }
  updateProductName(){
    let uu={"pid":this.pid,"pname":this.pname,"price":this.price,"quantity":this.prodquantity,"url":this.url}
    this.productdis. updateProductDetails(uu).subscribe(res=>this.updateMsg=res, 
   ()=>{
        this.loadProduct()
      })
      this.flag=false
   }
   addProduct(){
   this.flag1=true
  }
  addProductdetails(productRef:NgForm){
    console.log(productRef.value)
    this.productdis.storProductDetails(productRef.value).subscribe(res=>this.productMsg=res,
      ()=>{
        this.loadProduct()
      })
      this.flag=false

  }

}
