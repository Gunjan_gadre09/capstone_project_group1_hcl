import { Component, OnInit } from '@angular/core';
import { Addcart } from '../addcart';
import { AddcartserviceService } from '../addcartservice.service';
import { Product } from '../product';
import { ProductserviceService } from '../productservice.service';
import { Wishlist } from '../wishlist';
import { WishlistserviceService } from '../wishlistservice.service';

@Component({
  selector: 'app-after-user-login',
  templateUrl: './after-user-login.component.html',
  styleUrls: ['./after-user-login.component.css']
})
export class AfterUserLoginComponent implements OnInit {

  constructor(public productdis:ProductserviceService,public wishlist:WishlistserviceService,public addcart:AddcartserviceService) { }
  product:Array<Product>=[];
  cartList:Array<Addcart>=[];
  wishList:Array<Wishlist>=[];
  deleteMsg:string=""
  updateMsg:string=""
  productMsg:string=""
  flag:boolean=false
  flag1:boolean=false
  flag2:boolean=false
  flag3:boolean=false
  pid:number=0
  pname:string=""
  price:number=0
  prodquantity:number=0
  prodcategory:string=""
  url:string=""
  aWishlist:string=""
  dd:Date=new Date()
  acart:string=""
 
  name=sessionStorage.getItem("obj")
  ngOnInit(): void {
    this.loadProduct()
    this.loadCart()
    this.loadWishlist()
  }
  loadProduct():void{
    console.log("event fired")
    this.productdis.LoadProductDetails().subscribe(res=>{
      this.product=res
      console.log(res)
    })
  }

  loadCart():void{
    console.log("event fired")
    this.addcart.LoadcartDetails().subscribe(res=>{
      this.cartList=res
      console.log(res)
    })
  }
  loadWishlist():void{
    console.log("event fired")
    this.wishlist.LoadwishDetails().subscribe(res=>{
      this.wishList=res
      console.log(res)
    })
  }



  addCartProduct(uu:Product){
    console.log(uu)
    this.flag=true
    this.pid=uu.pid
    this.pname=uu.pname
    this.price=uu.price
    this.prodcategory=uu.prodtype
    this.prodquantity=uu.prodquantity
    this.url=uu.url
    
  }
  addcartProductName(){
    let uu={"pid":this.pid,"pname":this.pname,"price":this.price,"proquantity":1,"prodcategory":this.prodcategory,"url":this.url,"date":this.dd,"username":this.name}
   console.log(uu)
    this.addcart.addcartlistdetails(uu).subscribe(res=>this.acart=res)
    this.flag=false
   
  }
  AddWishlistProduct(uu:Product){
   console.log(uu)
   console.log(uu)
   this.flag3=true
   this.pid=uu.pid
   this.pname=uu.pname
   this.price=uu.price
   this.prodcategory=uu.prodtype
   this.url=uu.url
   
  }
  addWishList(){
    let uu={"pid":this.pid,"pname":this.pname,"price":this.price,"prodcategory":this.prodcategory,"url":this.url,"username":this.name}
    console.log(uu)
    this.flag3=false
    this.wishlist.addWishlistdetails(uu).subscribe(res=>{
      this.aWishlist=res;
  console.log(res)
})
  }

  deleteWishlist(pid:number){
    console.log(pid)
    this.wishlist.deleteWishDetail(pid).subscribe(res=>this.deleteMsg=res)
   
  }
  deleteCart(pid:number){
    console.log(pid)
    this.addcart.deleteProductDetails(pid).subscribe(res=>this.deleteMsg=res)
  }

  showWishList(){
    this.flag1=true
  }

  showlessProduct(){
    this.flag1=false
  }

  showcartList(){
    this.flag2=true
  }
  showlessProductcart(){
    this.flag2=false
  }
}