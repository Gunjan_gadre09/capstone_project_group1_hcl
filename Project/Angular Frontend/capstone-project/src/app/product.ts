export class Product {
    constructor(
        public pid:number,
        public pname:string,
        public price:number,
        public prodtype:string,
        public prodquantity:number,
        public url:string
    ){}
}
