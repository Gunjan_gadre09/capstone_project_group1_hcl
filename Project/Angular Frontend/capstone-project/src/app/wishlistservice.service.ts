import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Wishlist } from './wishlist';

@Injectable({
  providedIn: 'root'
})
export class WishlistserviceService {

  constructor(public http:HttpClient) { }
  addWishlistdetails(wishlist:any):Observable<string>{
    return this.http.post("http://localhost:8282/wishlist/storewishlist",wishlist,{responseType:'text'})
  }
  LoadwishDetails():Observable<Wishlist[]>{
    return this.http.get<Wishlist[]>("http://localhost:8282/wishlist/getallProductfromWL")
  }
  deleteWishDetail(pid:number):Observable<string>{
    return this.http.delete("http://localhost:8282/wishlist/deleteproduct/"+pid,{responseType:'text'})
  }
}
