import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminserviceService } from '../adminservice.service';

@Component({
  selector: 'app-adminsignup',
  templateUrl: './adminsignup.component.html',
  styleUrls: ['./adminsignup.component.css']
})
export class AdminsignupComponent implements OnInit {

  constructor(public admin:AdminserviceService ,public router:Router) { }
  adminMsg:string=""
  ngOnInit(): void {
  }
  AdminSignUp(adminRef:NgForm){
    console.log(adminRef.value)
    this.admin.AdminSignupDetails(adminRef.value).subscribe(res=>{
      this.adminMsg=res;
      console.log(res)
      this.router.navigate(["adminlogin"])
    })
   // this.router.navigate(["adminlogin"])
  }
}
