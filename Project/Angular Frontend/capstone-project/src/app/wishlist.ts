export class Wishlist {
    constructor(
        public pid:number,
        public pname:string,
        public price:number,
        public prodtype:string,
        public prodcategory:string,
        public username:string,
        public url:string
    ){}
}
