import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-usersignup',
  templateUrl: './usersignup.component.html',
  styleUrls: ['./usersignup.component.css']
})
export class UsersignupComponent implements OnInit {

  constructor( public user:UserserviceService,public router:Router) { }
  userMsg:string=""
  ngOnInit(): void {
  }
  UserSignUp(userRef:NgForm){
    console.log(userRef.value)
    this.user.UserSignUpDetails(userRef.value).subscribe(res=>{
      this.userMsg=res;
      if(res="done"){
    this.router.navigate(["userlogin"])
      }
    })
  }
}
