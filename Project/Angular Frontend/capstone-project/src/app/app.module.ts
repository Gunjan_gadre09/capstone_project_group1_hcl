import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserloginComponent } from './userlogin/userlogin.component';
import { UsersignupComponent } from './usersignup/usersignup.component';
import { AdminsignupComponent } from './adminsignup/adminsignup.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserCrudComponent } from './user-crud/user-crud.component';
import { ProductCrudComponent } from './product-crud/product-crud.component';
import { GetStockComponent } from './get-stock/get-stock.component';
import { GetSalesReportComponent } from './get-sales-report/get-sales-report.component';
import { AdminOperationComponent } from './admin-operation/admin-operation.component';
import { AfterUserLoginComponent } from './after-user-login/after-user-login.component';

@NgModule({
  declarations: [
    AppComponent,
    UserloginComponent,
    UsersignupComponent,
    AdminsignupComponent,
    AdminloginComponent,
    UserCrudComponent,
    ProductCrudComponent,
    GetStockComponent,
    GetSalesReportComponent,
    AdminOperationComponent,
    AfterUserLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,ReactiveFormsModule,FormsModule,HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
