import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Users } from './users';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(public http:HttpClient ) {}
UserSignUpDetails(user:UserserviceService):Observable<string>{
  return this.http.post("http://localhost:8282/user/register" ,user, {responseType:'text'})
}
LoaduserDetails():Observable<Users[]>{
  return this.http.get<Users[]>("http://localhost:8282/user/getuser")
}
updateuserDetails(users:any):Observable<string>{
  return this.http.patch("http://localhost:8282/user/updateuser",users,{responseType:'text'})
}
userLoginDetails(user:UserserviceService):Observable<string>{
  return this.http.post("http://localhost:8282/user/login",user,{responseType:'text'})
 }
deleteUser(uid:number):Observable<any>{
  return this.http.delete("http://localhost:8282/user/deleteuser/"+uid,{responseType:'text'})
}
}
