import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Addcart } from './addcart';

@Injectable({
  providedIn: 'root'
})
export class AddcartserviceService {

  constructor(public http:HttpClient) { }
  addcartlistdetails(cartlist:any):Observable<string>{
    return this.http.post("http://localhost:8282/AddCart/storeInCart",cartlist,{responseType:'text'})
  }
  LoadcartDetails():Observable<Addcart[]>{
    return this.http.get<Addcart[]>("http://localhost:8282/AddCart/getallProductFromCart")
  }
  deleteProductDetails(pid:number):Observable<string>{
    return this.http.delete("http://localhost:8282/AddCart/deleteproduct/"+pid,{responseType:'text'})
  }
}
