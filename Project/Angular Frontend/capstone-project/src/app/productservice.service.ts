import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductserviceService {

  constructor(public http:HttpClient ) { }
  LoadProductDetails():Observable<Product[]>{
    return this.http.get<Product[]>("http://localhost:8282/product/getallProduct")
  }
  updateProductDetails(product:any):Observable<string>{
    return this.http.patch("http://localhost:8282/user/updateuser",product,{responseType:'text'})
  }
  deleteProductDetails(pid:number):Observable<string>{
    return this.http.delete("http://localhost:8282/product/deleteproduct/"+pid,{responseType:'text'})
  }
  storProductDetails(product:ProductserviceService):Observable<string>{
    return this.http.post("http://localhost:8282/product/store",product,{responseType:'text'})
  
  }
  
}
