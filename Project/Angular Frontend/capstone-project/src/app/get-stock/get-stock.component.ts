import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductserviceService } from '../productservice.service';

@Component({
  selector: 'app-get-stock',
  templateUrl: './get-stock.component.html',
  styleUrls: ['./get-stock.component.css']
})
export class GetStockComponent implements OnInit {

  constructor(public productdis:ProductserviceService) { }
  product:Array<Product>=[];
  ngOnInit(): void {
    this.loadProduct()
  }
  loadProduct():void{
    console.log("event fired")
    this.productdis.LoadProductDetails().subscribe(res=>this.product=res)
  }
}
