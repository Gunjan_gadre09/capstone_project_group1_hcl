export class Addcart {
    constructor(
        public pid:number,
        public pname:string,
        public price:number,
        public prodtype:string,
        public prodquantity:number,
        public prodcategory:string,
        public username:string,
        public url:string
       
    ){}
}
