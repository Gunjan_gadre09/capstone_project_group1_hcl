import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminOperationComponent } from './admin-operation/admin-operation.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AdminsignupComponent } from './adminsignup/adminsignup.component';
import { AfterUserLoginComponent } from './after-user-login/after-user-login.component';
import { GetSalesReportComponent } from './get-sales-report/get-sales-report.component';
import { GetStockComponent } from './get-stock/get-stock.component';
import { ProductCrudComponent } from './product-crud/product-crud.component';
import { UserCrudComponent } from './user-crud/user-crud.component';
import { UserloginComponent } from './userlogin/userlogin.component';
import { UsersignupComponent } from './usersignup/usersignup.component';

const routes: Routes = [
  {path:"userlogin",component:UserloginComponent},
  {path:"adminlogin",component:AdminloginComponent},
  {path:"usersignup",component:UsersignupComponent},
  {path:"adminsignup",component:AdminsignupComponent},
  {path:"user-crud",component:UserCrudComponent},
  {path:"product-crud",component:ProductCrudComponent},
  {path:"get-stock",component:GetStockComponent},
  {path:"get-Sales-report",component:GetSalesReportComponent},
  {path:"admin-operation",component:AdminOperationComponent},
  {path:"after-user-login",component:AfterUserLoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
