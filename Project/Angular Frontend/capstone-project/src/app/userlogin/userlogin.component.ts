import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {
  userLoginRef=new FormGroup({
    email:new FormControl(),
    passward:new FormControl()
  })
  constructor(public router:Router,public user:UserserviceService) { }
  userss:string="";
  ngOnInit(): void {
  }
  checkUserlogin(){
    let loginuser=this.userLoginRef.value
    console.log(loginuser)
    sessionStorage.setItem("obj",loginuser.email);
    this.user.userLoginDetails(loginuser).subscribe(res=>
      {
        this.userss=res;
        console.log(res)
        if(res=="Login Successfully"){
          this.router.navigate(["after-user-login"])
        }
      })
    }
}
