package com.greatlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@SpringBootApplication(scanBasePackages = "com.greatlearning")
@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.greatlearning.dao")
@EntityScan(basePackages = "com.greatlearning.bean")

public class UserMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserMicroServiceApplication.class, args);
		System.out.println("User service running on port number 8282");
	
	}

}
