package com.greatlearning.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Users {
	@Id
private int uid;
private String email;
private String passward;
private String name;
public int getUid() {
	return uid;
}
public void setUid(int uid) {
	this.uid = uid;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassward() {
	return passward;
}
public void setPassward(String passward) {
	this.passward = passward;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
@Override
public String toString() {
	return "Users [uid=" + uid + ", email=" + email + ", passward=" + passward + ", name=" + name + "]";
}


}
