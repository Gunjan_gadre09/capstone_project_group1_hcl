package com.greatlearning.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class WishList {
	@Id
	private int pid;
	private String pName;
	private String Price;
	private String ProdCategory;
	private String prodQuantity;
	private String UserName;
	private String url;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	public String getPrice() {
		return Price;
	}
	public void setPrice(String price) {
		Price = price;
	}
	public String getProdCategory() {
		return ProdCategory;
	}
	public void setProdCategory(String prodCategory) {
		ProdCategory = prodCategory;
	}
	public String getProdQuantity() {
		return prodQuantity;
	}
	public void setProdQuantity(String prodQuantity) {
		this.prodQuantity = prodQuantity;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	@Override
	public String toString() {
		return "WishList [pid=" + pid + ", pName=" + pName + ", Price=" + Price + ", ProdCategory=" + ProdCategory
				+ ", prodQuantity=" + prodQuantity + ", UserName=" + UserName + ", url=" + url + "]";
	}
	
	
}
