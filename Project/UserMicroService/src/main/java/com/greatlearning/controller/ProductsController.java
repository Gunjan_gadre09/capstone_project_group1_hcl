package com.greatlearning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.bean.Products;
import com.greatlearning.bean.Users;
import com.greatlearningservice.ProductService;

@RestController
@RequestMapping("product")
public class ProductsController {
	@Autowired
	ProductService productService;
	//for store product
			@PostMapping(value = "store",consumes = MediaType.APPLICATION_JSON_VALUE)
			public String storeProducts(@RequestBody Products pro) {
				return productService.storeProductDetails(pro);
			}
		
//		//for signin
//			@GetMapping(value = "login/{email}/{passward}")
//			public String loginUser(@PathVariable("email") String email ,@PathVariable("passward") String passward) {
//				return userService.userLoginDetails(email, passward);
//			}
			
		//delete user details based upon id
			@DeleteMapping(value = "deleteproduct/{pid}")
			public String deleteProductInfo(@PathVariable("pid") int pid) {
				return productService.deleteProduct(pid);
			}
					//update product
					@PatchMapping(value = "updateproduct")
					public String updateUserDetails(@RequestBody Products pro) {
						return productService.updateProductInfo(pro);
					}
								
					@GetMapping(value="getallProduct")
					public List<Products> getAllpro(){
						return productService.getAllProductDetails();
					}

}
