package com.greatlearning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.bean.AddCart;
import com.greatlearning.bean.Products;
import com.greatlearningservice.AddCartService;

@RestController
@RequestMapping("AddCart")
public class AddCartController {
	@Autowired 
	AddCartService addCartService;
	
	//for store product in cart
	@PostMapping(value = "storeInCart",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeProducts(@RequestBody AddCart cart) {
		return addCartService.storeProductInCart(cart);
	}

	
//delete user details based upon id
	@DeleteMapping(value = "deleteproductFromCart/{pid}")
	public String deleteProductFromCart(@PathVariable("pid") int pid) {
		return addCartService.deleteProductFromCart(pid);
	}
					
	@GetMapping(value="getallProductFromCart")
	public List<AddCart> getAllProFromCart(){
		return addCartService.getAllProductFromCart();
	}

	@GetMapping(value ="getProductFromCartByName/{uesrname}")
	public List<AddCart> getProductFromCartByName(@PathVariable("username") String username) {
		return addCartService.getAllProductFromCartByName(username);
	}

}
