package com.greatlearning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.bean.Users;
import com.greatlearningservice.UserService;

@RestController
@RequestMapping("user")
public class UserController {
	@Autowired
	UserService userService;
	
	
	//for user registration 
		@PostMapping(value = "register",consumes = MediaType.APPLICATION_JSON_VALUE)
		public String storeUserDetails(@RequestBody Users user) {
			return userService.createAccount(user);
		}
	
	//for signin
		@GetMapping(value = "login/{email}/{passward}")
		public String loginUser(@PathVariable("email") String email ,@PathVariable("passward") String passward) {
			return userService.userLoginDetails(email, passward);
		}
		
		//delete user details based upon id
				@DeleteMapping(value = "deleteuser/{uid}")
				public String deleteUserInfo(@PathVariable("uid") int uid) {
					return userService.deleteUser(uid);
				}
				
				@PatchMapping(value = "updateuser")
				public String updateUserDetails(@RequestBody Users uu) {
					return userService.updateUserInfo(uu);
				}
							
	
}
