package com.greatlearning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.bean.AddCart;
import com.greatlearning.bean.WishList;
import com.greatlearningservice.WishListService;
@RestController
@RequestMapping("wishlist")
public class WishListController {
	@Autowired
	WishListService wishListService;
	
	//for store product in wishlist
		@PostMapping(value = "storewishlist",consumes = MediaType.APPLICATION_JSON_VALUE)
		public String storeProductsInWishList(@RequestBody WishList pro) {
			return wishListService.storeProductInWishlist(pro);
		}

		
	//delete product from wishlist
		@DeleteMapping(value = "deleteproductFromWishlist/{pid}")
		public String deleteProductInfo(@PathVariable("pid") int pid) {
			return wishListService.deleteProductFromWishlistt(pid);
		}
			
	// display all wish list
	@GetMapping(value="getallProductfromWL")
	public List<WishList> getAllproFromWishlist(){
		return wishListService.getAllProductFromWishlist();
	}

	//
	@GetMapping(value ="getProductFromWishListByName/{uesrname}")
	public List<WishList> getProductFromCartByName(@PathVariable("username") String username) {
		return wishListService.getProductFromWishListByName(username);
	}

}
