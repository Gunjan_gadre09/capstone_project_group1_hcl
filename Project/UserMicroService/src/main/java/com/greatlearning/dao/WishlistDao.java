package com.greatlearning.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.AddCart;
import com.greatlearning.bean.WishList;

@Repository
public interface WishlistDao extends JpaRepository<WishList, Integer>{

	 @Query("select p from WishList p where p.userName=:userName")
		List<WishList> getByName(@Param("userName") String userName);
}
