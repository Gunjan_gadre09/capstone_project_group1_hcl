package com.greatlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.Users;
@Repository
public interface UserDao extends JpaRepository<Users, Integer>{
	 @Query("select p from Users p where p.email=:email and p.passward= :passward")
		Users existsByEmail(@Param("email") String email,@Param("passward") String passward) ;

	boolean existsByEmail(@Param("email") String email);



}
