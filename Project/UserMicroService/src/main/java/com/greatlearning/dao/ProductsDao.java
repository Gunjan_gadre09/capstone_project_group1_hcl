package com.greatlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.Products;

@Repository
public interface ProductsDao extends JpaRepository<Products, Integer>{

}
