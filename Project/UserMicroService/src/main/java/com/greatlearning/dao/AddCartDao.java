package com.greatlearning.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.AddCart;

@Repository
public interface AddCartDao extends JpaRepository<AddCart, Integer> {
	 @Query("select p from AddCart p where p.userName=:userName")
		List<AddCart> getByName(@Param("userName") String userName);

}
