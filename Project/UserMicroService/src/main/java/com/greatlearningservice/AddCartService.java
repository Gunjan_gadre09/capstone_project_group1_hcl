package com.greatlearningservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.greatlearning.bean.AddCart;
import com.greatlearning.dao.AddCartDao;

@Service
public class AddCartService {
@Autowired
AddCartDao addCartDao;

//for storing product in cart
		public String storeProductInCart(AddCart cart) {
			if(addCartDao.existsById(cart.getPid())) {
				
				return "This Product is already in your wish list";
			}else {
				addCartDao.save(cart);
				return "Product add to wishlist Successfully";
			}
		}
		
		//for getting all products from addcart for specific person
				public List<AddCart> getAllProductFromCartByName(String userName) {
					List<AddCart> listt=addCartDao.getByName(userName);
					if(listt==null){
						return null;
					}else {
					return listt;
					}
				}
		
		//for getting all products from addcart
		public List<AddCart> getAllProductFromCart() {
			List<AddCart> listt=addCartDao.findAll();
			if(listt==null){
				return null;
			}else {
			return addCartDao.findAll();
			}
		}
		
		// for deleting product from addcart
		public String deleteProductFromCart(int pid) {
			if(!addCartDao.existsById(pid)) {
				return "Product details not present";
				}else {
				addCartDao.deleteById(pid);
				return "Product remove from cart";
				}	
		}

		


}
