package com.greatlearningservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.AddCart;
import com.greatlearning.bean.WishList;
import com.greatlearning.dao.WishlistDao;

@Service
public class WishListService {
@Autowired 
WishlistDao wishlistDao;

//for storing product in wishlist
		public String storeProductInWishlist(WishList wlistt) {
			if(wishlistDao.existsById(wlistt.getPid())) {
				return "This Product is already in your wish list";
			}else {
				wishlistDao.save(wlistt);
				return "Product add to wishlist Successfully";
			}
		}
		//for getting all products from wishlist
		public List<WishList> getAllProductFromWishlist() {
			List<WishList> listt=wishlistDao.findAll();
			if(listt==null){
				return null;
			}else {
			return wishlistDao.findAll();
			}
		}
		
		// for deleting product from wishlistt
		public String deleteProductFromWishlistt(int pid) {
			if(!wishlistDao.existsById(pid)) {
				return "Product details not present in wist list";
				}else {
				wishlistDao.deleteById(pid);
				return "Product remove from wishlist";
				}	
		}
		//list of product by name
		public List<WishList> getProductFromWishListByName(String userName) {
			List<WishList> listt=wishlistDao.getByName(userName);
			if(listt==null){
				return null;
			}else {
			return listt;
			}
		}

}
