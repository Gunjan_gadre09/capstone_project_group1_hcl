package com.greatlearningservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.Users;
import com.greatlearning.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;
	
	//for user login
		public String userLoginDetails(String email, String passward) {
			Users ad=userDao.existsByEmail(email, passward);
			if(ad==null) {
				return "Account credentials are not match please try again";
			}else {
				return "Login Successfully";
			}
		}
		
		//for user registration
		public String createAccount(Users user) {
			if(userDao.existsById(user.getUid())) {
				return "This user is already present";
			}else {
				userDao.save(user);
				return "Users Registration done Successfully";
			}
		}
		
		public List<Users> getAllUserDetails() {
			return userDao.findAll();
		}
		
		public String deleteUser(int uid) {
			if(!userDao.existsById(uid)) {
				return "user details not present for this user";
				}else {
				userDao.deleteById(uid);
				return "User details deleted successfully";
				}	
		}

		
		public String updateUserInfo(Users uu) {
			if(!userDao.existsById(uu.getUid()) ){
				return "user details not present for this user";
				}else {
				Users u= userDao.getById(uu.getUid()) ;
				u.setName(uu.getName());						
				userDao.saveAndFlush(u);				
				return "user updated successfully";
				}	
		}
		

	}
