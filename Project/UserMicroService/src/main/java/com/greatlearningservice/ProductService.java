package com.greatlearningservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.Products;
import com.greatlearning.bean.Users;
import com.greatlearning.dao.ProductsDao;

@Service
public class ProductService {
@Autowired
ProductsDao productsDao;
////for user login
//		public String userLoginDetails(String email, String passward) {
//			Users ad=userDao.existsByEmail(email, passward);
//			if(ad==null) {
//				return "Account credentials are not match please try again";
//			}else {
//				return "Login Successfully";
//			}
//		}
		
		//for storing product
		public String storeProductDetails(Products pro) {
			if(productsDao.existsById(pro.getPid())) {
				return "This Product is already present";
			}else {
				productsDao.save(pro);
				return "Users Registration done Successfully";
			}
		}
		//for getting all products
		public List<Products> getAllProductDetails() {
			return productsDao.findAll();
		}
		// for deleting product by id
		public String deleteProduct(int pid) {
			if(!productsDao.existsById(pid)) {
				return "Product details not present for this user";
				}else {
				productsDao.deleteById(pid);
				return "Product details deleted successfully";
				}	
		}

		//for updating name and price
		public String updateProductInfo(Products pro) {
			if(!productsDao.existsById(pro.getPid()) ){
				return "product details not present";
				}else {
				Products product= productsDao.getById(pro.getPid()) ;
				product.setpName(null);
				product.setPrice(pro.getPrice());
				productsDao.saveAndFlush(product);				
				return "product updated successfully";
				}	
		}
		


}
