package com.greatlearning.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.greatlearning.bean.Admin;
import com.greatlearning.service.AdminService;
@RestController
@RequestMapping("admin")
public class AdminController {
	@Autowired
	AdminService adminService;
	@Autowired
	RestTemplate restTemplate;
	
	//for admin Registration
	@PostMapping(value = "registeradmin",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String registerAdmin(@RequestBody Admin ad) {
		return adminService.registerAdmin(ad);
	}
	
	//for Admin login
	@GetMapping(value = "loginadmin/{username}/{passward}")
	public String loginAdmin(@PathVariable("username") String username ,@PathVariable("passward") String passward) {
		return adminService.adminLoginDetails(username, passward);
	}

	
	//deleting user base on id
		@DeleteMapping(value="deleteuser/{uid}")
		public String deleteuser(@PathVariable("uid") int uid) {
			String Url="http://user-service:8282/user/deleteuser/"+uid;
			restTemplate.delete(Url);
			return "deleted operation work";
		}
		
		@PostMapping(value = "AddinUser",
				consumes = MediaType.APPLICATION_JSON_VALUE)
		public String addInUser(@RequestBody Object ad) {
			String Url="http://user-service:8282/user/Store/";
			ResponseEntity<String> user= restTemplate.postForEntity(Url, ad, String.class);
			return user.getBody();
		}
		
		
		//get user by admin    crud on users
		@GetMapping(value="getallusers")
		public List<Object> getAllUser(){
			String Url="http://user-service:8282/user/getuser";
			Object[] res = restTemplate.getForObject(Url, Object[].class);
			return Arrays.asList(res);
		}
		

}
