package com.greatlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.Admin;

@Repository
public interface AdminDao extends JpaRepository<Admin, Integer>{
	@Query("select p from Admin p where p.username=:username and p.passward= :passward")
	Admin existsByEmail(@Param("username") String username,@Param("passward") String passward) ;
}
