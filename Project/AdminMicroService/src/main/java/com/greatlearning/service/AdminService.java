package com.greatlearning.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.bean.Admin;
import com.greatlearning.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	//for admin registration
		public String registerAdmin(Admin ad) {
			adminDao.save(ad);
			return "Admin Registered Successfully";
	   }
		
		//for admin login
			public String adminLoginDetails(String username, String passward) {
				Admin ad=adminDao.existsByEmail(username, passward);
				if(ad==null) {
					return "Account credentials are not match please try again";
				}else {
					return "Login Successfully";
				}
			}

}
