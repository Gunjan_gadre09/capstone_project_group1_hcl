package com.greatlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = "com.greatlearning")
@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.greatlearning.dao")
@EntityScan(basePackages = "com.greatlearning.bean")
public class AdminMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminMicroServiceApplication.class, args);
		System.out.println("Admin micro service run at port number 8181");
	}
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
